import input from "@inquirer/input";

type InputConfig = Parameters<typeof input>[0];

/** Asks the user for a string. */
export async function askForText(message: string, defaultValue?: string): Promise<string> {
	const config: InputConfig = {
		message,
		validate(input) {
			if (input.length > 0) return true;
			return "Please enter a value";
		},
	};
	if (defaultValue) {
		config.default = defaultValue;
	}
	return await input(config);
}

/** Asks the user for a possibly empty string. */
export async function askForTextOrEmpty(message: string): Promise<string> {
	return await input({ message });
}

/** Asks the user for a URL string. */
export async function askForUrl(message: string): Promise<URL> {
	const value = await input({
		message,
		validate(input) {
			if (URL.canParse(input)) return true;
			return "Please enter a valid URL";
		},
	});
	return new URL(value);
}
