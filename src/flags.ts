import { hideBin } from "yargs/helpers";
import yargs from "yargs/yargs";

/**
 * The values of the command-line arguments the user passed in, if any.
 */
export const flags = await yargs(hideBin(process.argv))
	.option("source", {
		alias: "s",
		type: "string",
		description: "The URL of the Webtoon installment to consider",
	})
	.option("name", {
		alias: "n",
		type: "string",
		description: "The name of the folder in which to put the episode files",
	})
	.option("prefix", {
		alias: "p",
		type: "string",
		description: "The prefix to put directly before the part number in filenames.",
	})
	.option("info", {
		alias: "i",
		type: "boolean",
		description: "Only read out metadata, don't download",
		default: false,
	})
	.option("verbose", {
		alias: "v",
		type: "boolean",
		description: "Run with verbose logging",
		default: false,
	})
	.help("h")
	.parse();
