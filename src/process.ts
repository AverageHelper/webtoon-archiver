// Node won't catch the interrupt automatically, so we do it ourselves:

type ExitCallback = () => void;

const callbacks: Array<ExitCallback> = [];

process.on("SIGINT", function () {
	console.log("Caught interrupt signal");
	for (const callback of callbacks) {
		callback();
	}
	process.exit(130);
});

export function addCallback(callback: ExitCallback): void {
	callbacks.push(callback);
}
