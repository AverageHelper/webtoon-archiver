/** Logs the given message then quits with exit code 1. */
export function quit(message: string): never {
	console.error(message);
	process.exit(1);
}
