#!/usr/bin/env tsx

import { flags } from "./flags.js";
import { addCallback } from "./process.js";
import { askForText, askForTextOrEmpty, askForUrl } from "./ask.js";
import { createWriteStream } from "node:fs";
import { extname, join } from "node:path";
import { mkdir } from "node:fs/promises";
import { parse } from "node-html-parser";
import { quit } from "./quit.js";
import ora from "ora";

const { source, name, prefix, info, verbose } = flags;

if (source && !URL.canParse(source)) {
	quit("Please enter a valid URL");
}

const url = source
	? new URL(source)
	: await askForUrl("Where is the webtoon installment you wish to read?");

if (url.protocol !== "https:") {
	quit("Source URL must use the https: scheme. Please enter a different URL");
}

const lookup = ora("Looking up webtoon...").start();
const response = await fetch(url);
const webpage = await response.text();

lookup.start("Parsing webpage...");
const root = parse(webpage);

// ** Get the comic and episode titles **
const TITLE_SELECTOR = ".subj_info > a:first-of-type";
const comicTitleElement = root.querySelector(TITLE_SELECTOR);
if (!comicTitleElement) {
	quit(`No element found with selector '${TITLE_SELECTOR}'`);
}

const comicTitle = comicTitleElement.getAttribute("title")?.trim() ?? "";
if (!comicTitle) {
	quit("Could not find title for comic.");
}

lookup.succeed();
console.info(`Title: ${comicTitle}`);

const EPISODE_TITLE_SELECTOR = ".subj_info > h1:last-of-type";
const episodeTitleElement = root.querySelector(EPISODE_TITLE_SELECTOR);
if (!episodeTitleElement) {
	quit(`No element found with selector '${EPISODE_TITLE_SELECTOR}'`);
}

const episodeTitle = episodeTitleElement.getAttribute("title")?.trim() ?? "";
if (!episodeTitle) {
	quit("Could not find title for episode.");
}

console.info(`Episode: ${episodeTitle}`);

// ** Get the comic images**
const IMAGE_LIST_SELECTOR = "#_imageList";
const imageList = root.querySelector(IMAGE_LIST_SELECTOR);
if (!imageList) {
	quit(`No element found with selector '${IMAGE_LIST_SELECTOR}'`);
}

const IMAGE_LIST_ITEM_SELECTOR = "._images";
const images = imageList.querySelectorAll(IMAGE_LIST_ITEM_SELECTOR);
if (images.length === 0) {
	quit(`No elements in image list found with selector '${IMAGE_LIST_ITEM_SELECTOR}'`);
}

// The standard `src` attribute has a transparent image by default, likely as
// a measure against scraping. This attribute gets replaced at runtime with
// Javascript. Fortunately, Webtoon keeps the canonical URL statically
// in a different attribute on the same element:
const IMAGE_SOURCE_ATTRIBUTE = "data-url";
const urls = images
	.map(img => img.getAttribute(IMAGE_SOURCE_ATTRIBUTE) ?? "")
	.filter(src => {
		const result = URL.canParse(src);
		if (!result && verbose) {
			console.warn(`src attribute could not be parsed as a URL: '${src}'`);
		}
		return result;
	})
	.map(src => new URL(src));

if (urls.length === 0) {
	quit("No valid images found :(");
}

console.log(`Found ${urls.length} viable image URLs!`);

// Exit if the user has asked we do not download the images
if (info) process.exit(0);

// ** Download the episode **
// TODO: What to do if there's just one part?

const totalWidth = `${urls.length}`.length;

const episodeFolderName =
	name ||
	(await askForText(
		`What would you like to name this episode folder? (e.g. 'Episode 11')`,
		episodeTitle
	));

// TODO: Ask the user where to save the episode
// TODO: Be careful about path arguments...
const episodeFolder = new URL(`../data/${episodeFolderName}`, import.meta.url); // TODO: Relative to working directory, not module directory
const folderSpinner = ora({
	text: `Creating folder at ${decodeURIComponent(episodeFolder.pathname)}`,
	isSilent: !verbose,
});
addCallback(() => {
	// Ora hides the cursor, and interrupt won't restore it automatically
	if (!folderSpinner.isSpinning) return;
	folderSpinner.fail();
});
folderSpinner.start();
await mkdir(episodeFolder, { recursive: true }); // <data>/Episode 11/
folderSpinner.succeed();

const episodePrefix =
	prefix ?? (await askForTextOrEmpty("What would you like to prefix each part? (e.g. 's02e11')"));

const progress = ora(`Downloading ${urls.length} images...`);
addCallback(() => {
	if (!progress.isSpinning) return;
	progress.fail();
});

progress.start();
for (const [index, url] of urls.entries()) {
	const percent = Math.floor((index / urls.length) * 100);
	progress.start(`(${percent}%) Downloading file ${index + 1} of ${urls.length}...`);

	const episodePartIndex = `${index + 1}`.padStart(totalWidth, "0");
	const fileExtension = extname(url.href);
	const filename = `${episodePrefix}${episodePartIndex}${fileExtension}`;

	const partFile = new URL(join(`../data/${episodeFolderName}`, filename), import.meta.url);

	const result = await fetch(url); // TODO: Run downloads 4 at a time
	const data = await result.arrayBuffer();
	if (verbose) {
		progress.info(`Downloaded file ${index + 1} from ${url.href}`);
	}

	createWriteStream(partFile).write(Buffer.from(data)); // <data>/Episode 11/01.png
	if (verbose) {
		progress.info(`File ${index + 1} written to ${decodeURIComponent(partFile.pathname)}`);
	}
}

progress.succeed(
	`${urls.length} images downloaded to ${decodeURIComponent(episodeFolder.pathname)}`
);
