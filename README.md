# Webtoon Archiver

In an effort to preserve stories against various kinds of bit rot, this project aims to make accessible the archival of webcomic images.

This script is very simple, not much better than right-clicking on images in your browser and selecting "save as". It uses the same mechanism as your browser to download images, albeit in a methodically organized way. (This is not a very efficient script, but it does the trick, lol.)

You might consider reviewing the downloaded images after running this script. This script assumes the image order in the DOM is canonical, but I wouldn't put it past Webtoon to break that assumption in the future. (Already they do Javascript shenanigans to hide the standard `src` attribute on load.)

Please note: This project is for _archival_ purposes only. Please support the authors and artists of the comics you archive. If the authors sell their own DRM-free digital version of the comic, please purchase that instead of using this tool.

## Prerequisites

Clone the project. Then, install dependencies:

```sh
npm ci
```

## Interactive Usage

Run the script with `npm start` or `npx tsx .`

1. The script will ask you for a Webtoons _episode_ URL (note: _not a whole comic URL_).
2. Then the script will parse the webpage, and tell you how many parts are contained within.
3. If you choose to proceed to downloading the episode, the script will ask for the folder name and file prefix to use.
4. The resulting files will be stored in the `<repository root>/data` folder for you to do with as you please!

## CLI Usage

Run the script with `npm start -- [...args]` or `npx tsx . [...args]`

The command-line arguments are as follows:

- `--source` or `-s`: The URL to consider. This should be a Webtoon episode link or similar, _not a link to the whole comic_.
- `--name` or `-n`: The name of the folder to put the episode contents into.
  - Omitting this argument means the script should ask for the value interactively.
- `--prefix` or `-p`: A string to use at the start of filenames as they're saved.
  - For example, if the value `'s02e11 '` is passed for this option, then the resulting files will be named `s02e11 01.png`, `s02e11 02.png`, and so on.
  - Whitespace is considered in this string.
  - Passing an empty string means no prefix.
  - Omitting this argument means the script should ask for the value interactively.
- `--info` or `-i`: Only fetch information about the installment, such as the comic's name, the episode's name, and the number of images the episode contains.
  - Omitting this argument or setting it `false` means all images should be downloaded.
- `--verbose` or `-v`: Run with verbose logging. This includes all download URLs and file URLs.
- `--help` or `-h`: Read out all command arguments.
- `--version`: Read out the script version, as found in `package.json`.

The script will ask interactively for more details if any are not provided in CLI args.

## Examples

To get information about a Webtoon comic episode:

```sh
npm start -- -is '<the url>'
```

To download an episode to `webtoon-archiver/data/Episode 21/xx.png`:

```sh
npm start -- -s '<the url>' -n 'Episode 21' -p ''
```

To download an episode to `webtoon-archiver/data/Episode 21/s02e21 xx.png`:

```sh
npm start -- -s '<the url>' -n 'Episode 21' -p 's02e21 '
```

To run the script interactively:

```sh
npm start
```
